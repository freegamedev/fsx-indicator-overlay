﻿using SharpDX;
using SharpDX.Direct2D1;

class cSpeed_Indicator
{

    public Bitmap _bmpBack;
    private Bitmap _bmpHand;

    Vector2 center;
    float _angle = 0f;
    float _scale = 1f;
    Vector2 _position;
    public Vector2 position { get { return _position; } set { _position = value; } }
    public float AirSpeed { get { return _angle; }
        set {
            if (value <= 23f)
                _angle = 0f;
            else if (value <= 160f)
                _angle = (value - 40f) * 2f + 30f;// the values are a little off on the guage drawing....
            else if (value <= 310f)
                _angle = value +110f;
            else
                _angle = 310f;
        }
    }
    public float scale { get { return _scale; } set { _scale = value; } }
    public float WidthScaled { get { return _bmpBack.Size.Width * _scale; } }
    public float HeightScaled { get { return _bmpBack.Size.Height * _scale; } }

    cMath math = new cMath();
    public cSpeed_Indicator(RenderTarget device, float Scale)
    {
        _scale = Scale;
        _bmpBack = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.airspeed_markings);
        _bmpHand = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.airspeed_hand);
        center = new Vector2(_bmpBack.Size.Width / 2f, _bmpBack.Size.Height / 2f);
    }

    public void Draw(RenderTarget device)
    {
        device.Transform = Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpBack, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Rotation(MathUtil.DegreesToRadians(_angle), center) * Matrix3x2.Translation(_position.X, _position.Y) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpHand, 1, BitmapInterpolationMode.Linear);

    }
}