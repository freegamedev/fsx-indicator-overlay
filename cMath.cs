﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class cMath
{
    public float InterpolPhyToAngle(float phyVal, float minPhy, float maxPhy, float minAngle, float maxAngle)
    {
        float a;
        float b;
        float y;
        float x;

        if (phyVal < minPhy)
        {
            return (float)(minAngle);// * Math.PI / 180);
        }
        else if (phyVal > maxPhy)
        {
            return (float)(maxAngle);// * Math.PI / 180);
        }
        else
        {

            x = phyVal;
            a = (maxAngle - minAngle) / (maxPhy - minPhy);
            b = (float)(0.5 * (maxAngle + minAngle - a * (maxPhy + minPhy)));
            y = a * x + b;

            return (float)(y);// * Math.PI / 180);
        }
    }
}
