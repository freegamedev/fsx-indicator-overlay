﻿using SharpDX;
using SharpDX.Direct2D1;

class cAttitude_Indicator
{

    public Bitmap _bmpBack;
    private Bitmap _bmpPitch;
    private Bitmap _bmpRoll1;
    private Bitmap _bmpForeground1;
    private Bitmap _bmpForeground2;

    Vector2 center;
    float _pitch = 0f;
    float _roll = 0f;
    float _scale = 1f;
    Vector2 _position;
    public float maxPitch = 25;
    public Vector2  position { get { return _position; } set { _position = value; } }
    public float pitch
    {
        get { return _pitch; }
        set
        {
            //pitch 1.35 pixels = 1 degrees
            if (value > maxPitch)
                _pitch = maxPitch * 1.35f * _scale;
            else if (value < -maxPitch)
                _pitch = -maxPitch * 1.35f * _scale;
            else
                _pitch = value * 1.35f * _scale; 
        }
    }
    public float roll { get { return _roll; } set { _roll = value; } }
    public float scale { get { return _scale; } set { _scale = value; } }
    public float WidthScaled { get { return _bmpBack.Size.Width * _scale; } }
    public float HeightScaled { get { return _bmpBack.Size.Height * _scale; } }
    public cAttitude_Indicator(RenderTarget device, float Scale)
    {
        _scale = Scale;
        _bmpBack = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.attitude_roll_1);
        _bmpPitch = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.attitude_pitch);
        _bmpRoll1 = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.attitude_roll_2);
        _bmpForeground1 = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.attitude_foreground_1);
        _bmpForeground2 = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.attitude_foreground_2);
        center = new Vector2(_bmpBack.Size.Width / 2, _bmpBack.Size.Height / 2);
    }

    public void Draw(RenderTarget device)
    {
        device.Transform = Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpBack, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Rotation(MathUtil.DegreesToRadians(_roll), center) * Matrix3x2.Translation(_position.X, _position.Y + _pitch / _scale) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpPitch, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Rotation(MathUtil.DegreesToRadians(_roll), center) * Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpRoll1, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpForeground1, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpForeground2, 1, BitmapInterpolationMode.Linear);
    }
}
