﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

//Special Thanks to the following projects for the indicators
//https://github.com/sebmatton/jQuery-Flight-Indicators
//https://github.com/CDOT-EDX/Skyhawk-Flight-Instruments
//*** TRANSPARENT FORM SETUP ***
namespace FSX_Indicators
{
    public partial class TransparentForm : Form
    {
        cDirectX directx;
        #region stuff for form transparency
        #region DllImport
        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll")]
        static extern bool SetLayeredWindowAttributes(IntPtr hwnd, uint crKey, byte bAlpha, uint dwFlags);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        [DllImport("dwmapi.dll")]
        public static extern void DwmExtendFrameIntoClientArea(IntPtr hWnd, ref int[] pMargins);

        [DllImport("user32.dll")]
        private static extern IntPtr SetActiveWindow(IntPtr handle);
        #endregion

        #region styles
        public const UInt32 SWP_NOSIZE = 0x0001;
        public const UInt32 SWP_NOMOVE = 0x0002;
        public const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;
        public static IntPtr HWND_TOPMOST = new IntPtr(-1);
        private const int WS_EX_NOACTIVATE = 0x08000000;
        private const int WS_EX_TOPMOST = 0x00000008;
        private const int WM_ACTIVATE = 6;
        private const int WA_INACTIVE = 0;
        private const int WM_MOUSEACTIVATE = 0x0021;
        private const int MA_NOACTIVATEANDEAT = 0x0004;
        #endregion

        #region overrides
        protected override void OnPaint(PaintEventArgs e)
        {
            int[] marg = new int[] { 0, 0, Width, Height };
            DwmExtendFrameIntoClientArea(this.Handle, ref marg);
        }

        /// <summary>
        /// Used to not show up the form in alt-tab window. 
        /// Tested on Windows 7 - 64bit and Windows 10 64bit
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams pm = base.CreateParams;
                pm.ExStyle |= 0x80;
                pm.ExStyle |= WS_EX_TOPMOST; // make the form topmost
                pm.ExStyle |= WS_EX_NOACTIVATE; // prevent the form from being activated
                return pm;
            }
        }

        /// <summary>
        /// Makes the form unable to gain focus at all time, 
        /// which should prevent lose focus
        /// </summary>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_MOUSEACTIVATE)
            {
                m.Result = (IntPtr)MA_NOACTIVATEANDEAT;
                return;
            }
            if (m.Msg == WM_ACTIVATE)
            {
                if (((int)m.WParam & 0xFFFF) != WA_INACTIVE)
                    if (m.LParam != IntPtr.Zero)
                        SetActiveWindow(m.LParam);
                    else
                        SetActiveWindow(IntPtr.Zero);
            }
            else
            {
                base.WndProc(ref m);
            }
        }
        #endregion

        #region utility
        public System.Drawing.Rectangle GetScreen(Control referenceControl)
        {
            return Screen.FromControl(referenceControl).Bounds;
        }
        #endregion
        #endregion

        #region form
        public TransparentForm()
        {
            #region Setup Form Properties
            int initialStyle = GetWindowLong(this.Handle, -20);
            SetWindowLong(this.Handle, -20, initialStyle | 0x80000 | 0x20);
            SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
            OnResize(null);
            #endregion

            InitializeComponent();
            this.TransparencyKey = System.Drawing.Color.Black;
            this.BackColor = this.TransparencyKey;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            #region Setup Form Properties
            // You can write your own dimensions of the auto-mode if doesn't work properly.
            System.Drawing.Rectangle screen = GetScreen(this);
            this.Width = screen.Width;
            this.Height = screen.Height;

            this.DoubleBuffered = true; // reduce the flicker
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer |// reduce the flicker too
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.DoubleBuffer |
                ControlStyles.UserPaint |
                ControlStyles.Opaque |
                ControlStyles.ResizeRedraw |
                ControlStyles.SupportsTransparentBackColor, true);
            this.TopMost = true;
            this.Visible = true;
            #endregion

            directx = new cDirectX(this.Handle, this.Width, this.Height);

            directx.transparentColor = new SharpDX.Color((byte)this.BackColor.R, (byte)this.BackColor.G, (byte)this.BackColor.B) ;
            ConnectToSimConnect();
        }

        private void TransparentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CloseSimConnect();
        }
        #endregion

        #region FSX
        const int WM_USER_SIMCONNECT = 0x0402;
        cSimConnect simConnect;
        void ConnectToSimConnect()
        {
            if (simConnect != null)
                simConnect.Dispose();

            simConnect = null;
            simConnect = new cSimConnect(this.Handle);
            simConnect.OnShowMessage += new cSimConnect.ShowMessage(this.OnShowMessage);
            simConnect.OnAttitudeIndicatorChange += new cSimConnect.AttitudeIndicatorChange(this.OnAttitudeIndicatorChange);
            simConnect.OnVerticalSpeedChange += new cSimConnect.VerticalSpeedChange(this.OnVerticalSpeedChange);
            simConnect.OnAltitudeIndicatorChange += new cSimConnect.AltitudeIndicatorChange(this.OnAltitudeIndicatorChange);
            simConnect.OnSpeedChange += new cSimConnect.SpeedChange(this.OnSpeedChange);
            simConnect.OnTurnCoordinatorChange += new cSimConnect.TurnCoordinatorChange(this.OnTurnCoordinatorChange);
            simConnect.OnHeadingIndicatorChange += new cSimConnect.HeadingIndicatorChange(this.OnHeadingIndicatorChange);
            simConnect.Initialize("FSX Indicators");
        }

        void CloseSimConnect()
        {
            if (simConnect != null)
            {
                simConnect.OnShowMessage -= this.OnShowMessage;
                simConnect.OnAttitudeIndicatorChange -= this.OnAttitudeIndicatorChange;
                simConnect.OnVerticalSpeedChange -= this.OnVerticalSpeedChange;
                simConnect.OnAltitudeIndicatorChange -= this.OnAltitudeIndicatorChange;
                simConnect.OnSpeedChange -= this.OnSpeedChange;
                simConnect.OnTurnCoordinatorChange -= this.OnTurnCoordinatorChange;
                simConnect.OnHeadingIndicatorChange -= this.OnHeadingIndicatorChange;

                simConnect.Dispose();
                simConnect = null;
            }
        }

        #region FSX Callback
        // Simconnect client will send a win32 message when there is 
        // a packet to process. ReceiveMessage must be called to
        // trigger the events. This model keeps simconnect processing on the main thread.
        protected override void DefWndProc(ref Message m)
        {
            if (m.Msg == WM_USER_SIMCONNECT)
            {
                if (simConnect.connect != null)
                {
                    simConnect.connect.ReceiveMessage();
                }
            }
            else
            {
                base.DefWndProc(ref m);
            }
        }
        #endregion

        #region FSX Events
        private void OnShowMessage(string str)
        {
            Console.WriteLine(str);
        }

        private void OnAttitudeIndicatorChange(gAttitudeIndicator data)
        {
            //directx.DebugString = -data.PitchDegrees + " " + data.BankDegrees;
            directx.attitudeIndicator.pitch = (float)-data.PitchDegrees;
            directx.attitudeIndicator.roll = (float)data.BankDegrees;
        }

        private void OnVerticalSpeedChange(gVerticalSpeedIndicator data)
        {
           //directx.DebugString = data.VerticalSpeed.ToString();
            directx.verticalSpeedIndicator.vertical_speed = (float)data.VerticalSpeed;
        }

        private void OnAltitudeIndicatorChange(gAltitudeIndicator data)
        {
            //directx.DebugString = data.Altitude.ToString();
            directx.altitudeIndicator.Altitude = (float)data.Altitude;
        }

        private void OnSpeedChange(gSpeedIndicator data)
        {
            //directx.DebugString = data.Speed.ToString();
            directx.speedIndicator.AirSpeed = (float)data.Speed;
        }

        private void OnTurnCoordinatorChange(gTurnCoordinator data)
        {
            //directx.DebugString = data.Plane + " " + data.Ball;
            directx.turnCoordinator.ball = (float)-data.Ball;
            directx.turnCoordinator.plane = (float)data.Plane;
        }

        private void OnHeadingIndicatorChange(gHeadingIndicator data)
        {
            //directx.DebugString = data.Heading + " " + data.Bug;
            directx.headingIndicator.Heading= (float)-data.Heading;
            directx.headingIndicator.HeadingBug = (float)data.Bug;
        }
        #endregion

        #endregion

        private void TransparentForm_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Close();
        }
    }
}
