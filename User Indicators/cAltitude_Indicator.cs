﻿using SharpDX;
using SharpDX.Direct2D1;

//InHg and Mbar images there but not implemented with logic yet...
class cAltitude_Indicator
{

    public Bitmap _bmpInHg;
    public Bitmap _bmpMbar;
    private Bitmap _bmpBack;
    private Bitmap _bmpHand1;
    private Bitmap _bmpHand2;
    private Bitmap _bmpHand3;

    Vector2 center;
    float _angle1 = 0f;
    float _angle2 = 0f;
    float _angle3 = 0f;
    float _altitude = 1f;
    float _scale = 1f;
    Vector2 _position;
    public Vector2 position { get { return _position; } set { _position = value; } }
    public float Altitude
    {
        get { return _altitude; }
        set
        {
            _angle1 = math.InterpolPhyToAngle(value, 0, 100000, 0, 359);   //tiny
            _angle2 = math.InterpolPhyToAngle(value, 0, 10000, 0, 359);     //small
            _angle3 = math.InterpolPhyToAngle(value % 1000, 0, 1000, 0, 359); //long
        }
    }
    public float scale { get { return _scale; } set { _scale = value; } }
    public float WidthScaled { get { return _bmpInHg.Size.Width * _scale; } }
    public float HeightScaled { get { return _bmpInHg.Size.Height * _scale; } }

    cMath math = new cMath();
    public cAltitude_Indicator(RenderTarget device, float Scale)
    {
        _scale = Scale;
        _bmpInHg = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.altimeter_pressure_inhg);
        _bmpMbar = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.altimeter_pressure_mbar);
        _bmpBack = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.altimeter_background);
        _bmpHand1 = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.altimeter_hand_10000ft );
        _bmpHand2 = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.altimeter_hand_1000ft);
        _bmpHand3 = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.altimeter_hand_100ft);
        center = new Vector2(_bmpInHg.Size.Width / 2f, _bmpInHg.Size.Height / 2f);
    }

    public void Draw(RenderTarget device)
    {
        device.Transform = Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpInHg, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpMbar, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpBack, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Rotation(MathUtil.DegreesToRadians(_angle1), center) * Matrix3x2.Translation(_position.X, _position.Y) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpHand1, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Rotation(MathUtil.DegreesToRadians(_angle2), center) * Matrix3x2.Translation(_position.X, _position.Y) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpHand2, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Rotation(MathUtil.DegreesToRadians(_angle3), center) * Matrix3x2.Translation(_position.X, _position.Y) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpHand3, 1, BitmapInterpolationMode.Linear);
    }
}

