﻿using Microsoft.FlightSimulator.SimConnect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

class cSimConnect : IDisposable
{
    public bool paused = false;
    IntPtr hWnd;
    const int WM_USER_SIMCONNECT = 0x0402;

    #region Decleration Indicators and Events
    public delegate void ShowMessage(string str);
    public event ShowMessage OnShowMessage;

    public delegate void AttitudeIndicatorChange(gAttitudeIndicator data);
    public event AttitudeIndicatorChange OnAttitudeIndicatorChange;
    public gAttitudeIndicator attitudeIndicator =new gAttitudeIndicator();

    public delegate void VerticalSpeedChange(gVerticalSpeedIndicator data);
    public event VerticalSpeedChange OnVerticalSpeedChange;
    public gVerticalSpeedIndicator verticalSpeedIndicator = new gVerticalSpeedIndicator();

    public delegate void AltitudeIndicatorChange(gAltitudeIndicator data);
    public event AltitudeIndicatorChange OnAltitudeIndicatorChange;
    public gAltitudeIndicator altitudeIndicator = new gAltitudeIndicator();

    public delegate void SpeedChange(gSpeedIndicator data);
    public event SpeedChange OnSpeedChange;
    public gSpeedIndicator speedIndicator = new gSpeedIndicator();

    public delegate void TurnCoordinatorChange(gTurnCoordinator data);
    public event TurnCoordinatorChange OnTurnCoordinatorChange;
    public gTurnCoordinator turnCoordinator = new gTurnCoordinator();

    public delegate void HeadingIndicatorChange(gHeadingIndicator data);
    public event HeadingIndicatorChange OnHeadingIndicatorChange;
    public gHeadingIndicator headingIndicator = new gHeadingIndicator();
    #endregion
    #region Construct/Dispose
    public SimConnect connect = null;
    public cSimConnect(IntPtr HWND)
    {
        hWnd = HWND;
    }
    ~cSimConnect()
    {
        this.Dispose(false);
    }
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
            // free managed resources
            CloseConnection();
            if (connect !=null)
            {
                connect.Dispose();
                connect = null;
            }
        }
        // free native resources if there are any.
        if (connect != null)
        {
            //    connect = null;
        }
    }
    #endregion
    #region Open and Close SimConnect connection
    bool OpenConnection(string name)
    {
        if (connect == null)
        {
            try
            {
                // the constructor is similar to simconnect.Open in the native API
                connect = new SimConnect(name, hWnd, WM_USER_SIMCONNECT, null, 0);
                return true;
            }
            catch (COMException ex)
            {
                OnShowMessage("Unable to connect to FSX (" + ex.Message + ")");
            }
            return false;
        }
        else
        {
            OnShowMessage("Already Connected? Trying to Close connection... - try again -");
            CloseConnection();
            return false;
        }
    }
    void CloseConnection()
    {
        if (connect != null)
        {
            //UnsubscribeFromSystemEvent();
        }
    }
    #endregion

    public void Initialize(string name)
    {
        if (OpenConnection(name))
        {
            CreateEventHandlers();
            AddToDataDefinitions();
            SubscribeToSystemEvents();
            OnShowMessage("Connected");
            //the next line just gets things moving. :)
            connect.RequestDataOnSimObject(DATA_REQUEST_ID.ATTITUDE_INDICATOR, DATA_DEFINE_ID.ATTITUDE_INDICATOR, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD.VISUAL_FRAME, SIMCONNECT_DATA_REQUEST_FLAG.CHANGED, (uint)0, (uint)0, (uint)0);
            connect.RequestDataOnSimObject(DATA_REQUEST_ID.VERTICAL_SPEED_INDICATOR, DATA_DEFINE_ID.VERTICAL_SPEED_INDICATOR, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD.VISUAL_FRAME, SIMCONNECT_DATA_REQUEST_FLAG.CHANGED, (uint)0, (uint)0, (uint)0);
            connect.RequestDataOnSimObject(DATA_REQUEST_ID.ALTITUDE_INDICATOR, DATA_DEFINE_ID.ALTITUDE_INDICATOR, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD.VISUAL_FRAME, SIMCONNECT_DATA_REQUEST_FLAG.CHANGED, (uint)0, (uint)0, (uint)0);
            connect.RequestDataOnSimObject(DATA_REQUEST_ID.SPEED_INDICATOR, DATA_DEFINE_ID.SPEED_INDICATOR, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD.VISUAL_FRAME, SIMCONNECT_DATA_REQUEST_FLAG.CHANGED, (uint)0, (uint)0, (uint)0);
            connect.RequestDataOnSimObject(DATA_REQUEST_ID.TURN_COORDINATOR, DATA_DEFINE_ID.TURN_COORDINATOR, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD.VISUAL_FRAME, SIMCONNECT_DATA_REQUEST_FLAG.CHANGED, (uint)0, (uint)0, (uint)0);
            connect.RequestDataOnSimObject(DATA_REQUEST_ID.HEADING_INDICATOR, DATA_DEFINE_ID.HEADING_INDICATOR, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD.VISUAL_FRAME, SIMCONNECT_DATA_REQUEST_FLAG.CHANGED, (uint)0, (uint)0, (uint)0);
        }
        else
        {
            OnShowMessage("Can't Connect");
        }
    }

    void CreateEventHandlers()
    {
        try
        {
            #region setup to listen to simconnect events
            connect.OnRecvAirportList += new SimConnect.RecvAirportListEventHandler(simconnect_OnRecvAirportList);
            connect.OnRecvAssignedObjectId += new SimConnect.RecvAssignedObjectIdEventHandler(simconnect_OnRecvAssignedObjectId);
            connect.OnRecvClientData += new SimConnect.RecvClientDataEventHandler(simconnect_OnRecvClientData);
            connect.OnRecvCustomAction += new SimConnect.RecvCustomActionEventHandler(simconnect_OnRecvCustomAction);
            connect.OnRecvEvent += new SimConnect.RecvEventEventHandler(simconnect_OnRecvEvent);
            connect.OnRecvEventFilename += new SimConnect.RecvEventFilenameEventHandler(simconnect_OnRecvEventFilename);
            connect.OnRecvEventFrame += new SimConnect.RecvEventFrameEventHandler(simconnect_OnRecvEventFrame);
            connect.OnRecvEventMultiplayerClientStarted += new SimConnect.RecvEventMultiplayerClientStartedEventHandler(simconnect_OnRecvEventMultiplayerClientStarted);
            connect.OnRecvEventMultiplayerServerStarted += new SimConnect.RecvEventMultiplayerServerStartedEventHandler(simconnect_OnRecvEventMultiplayerServerStarted);
            connect.OnRecvEventMultiplayerSessionEnded += new SimConnect.RecvEventMultiplayerSessionEndedEventHandler(simconnect_OnRecvEventMultiplayerSessionEnded);
            connect.OnRecvEventObjectAddremove += new SimConnect.RecvEventObjectAddremoveEventHandler(simconnect_OnRecvEventObjectAddremove);
            connect.OnRecvEventRaceEnd += new SimConnect.RecvEventRaceEndEventHandler(simconnect_OnRecvEventRaceEnd);
            connect.OnRecvEventRaceLap += new SimConnect.RecvEventRaceLapEventHandler(simconnect_OnRecvEventRaceLap);
            connect.OnRecvEventWeatherMode += new SimConnect.RecvEventWeatherModeEventHandler(simconnect_OnRecvEventWeatherMode);
            connect.OnRecvException += new SimConnect.RecvExceptionEventHandler(simconnect_OnRecvException);
            connect.OnRecvNdbList += new SimConnect.RecvNdbListEventHandler(simconnect_OnRecvNdbList);
            connect.OnRecvNull += new SimConnect.RecvNullEventHandler(simconnect_OnRecvNull);
            connect.OnRecvOpen += new SimConnect.RecvOpenEventHandler(simconnect_OnRecvOpen);
            connect.OnRecvQuit += new SimConnect.RecvQuitEventHandler(simconnect_OnRecvQuit);
            connect.OnRecvReservedKey += new SimConnect.RecvReservedKeyEventHandler(simconnect_OnRecvReservedKey);
            connect.OnRecvSimobjectData += new SimConnect.RecvSimobjectDataEventHandler(simconnect_OnRecvSimobjectData);
            connect.OnRecvSimobjectDataBytype += new SimConnect.RecvSimobjectDataBytypeEventHandler(simconnect_OnRecvSimobjectDataBytype);
            connect.OnRecvSystemState += new SimConnect.RecvSystemStateEventHandler(simconnect_OnRecvSystemState);
            connect.OnRecvVorList += new SimConnect.RecvVorListEventHandler(simconnect_OnRecvVorList);
            connect.OnRecvWaypointList += new SimConnect.RecvWaypointListEventHandler(simconnect_OnRecvWaypointList);
            connect.OnRecvWeatherObservation += new SimConnect.RecvWeatherObservationEventHandler(simconnect_OnRecvWeatherObservation);
            #endregion
        }
        catch (COMException ex)
        {
            OnShowMessage(ex.ToString());
        }
    }

    #region On Event Methods (Here is where the received data goes)
    private void simconnect_OnRecvWeatherObservation(SimConnect sender, SIMCONNECT_RECV_WEATHER_OBSERVATION data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvWaypointList(SimConnect sender, SIMCONNECT_RECV_WAYPOINT_LIST data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvVorList(SimConnect sender, SIMCONNECT_RECV_VOR_LIST data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvSystemState(SimConnect sender, SIMCONNECT_RECV_SYSTEM_STATE data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvSimobjectDataBytype(SimConnect sender, SIMCONNECT_RECV_SIMOBJECT_DATA_BYTYPE data)
    {
    }
    private void simconnect_OnRecvSimobjectData(SimConnect sender, SIMCONNECT_RECV_SIMOBJECT_DATA data)
    {
        switch ((DATA_REQUEST_ID)data.dwRequestID)
        {
            case DATA_REQUEST_ID.ATTITUDE_INDICATOR:
                attitudeIndicator = (gAttitudeIndicator)data.dwData[0];
                if (!paused)
                    OnAttitudeIndicatorChange(attitudeIndicator);
                break;
            case DATA_REQUEST_ID.VERTICAL_SPEED_INDICATOR:
                verticalSpeedIndicator = (gVerticalSpeedIndicator)data.dwData[0];
                if (!paused)
                    OnVerticalSpeedChange(verticalSpeedIndicator);
                break;
            case DATA_REQUEST_ID.ALTITUDE_INDICATOR:
                altitudeIndicator = (gAltitudeIndicator)data.dwData[0];
                if (!paused)
                    OnAltitudeIndicatorChange(altitudeIndicator);
                break;
            case DATA_REQUEST_ID.SPEED_INDICATOR:
                speedIndicator = (gSpeedIndicator)data.dwData[0];
                if (!paused)
                    OnSpeedChange(speedIndicator);
                break;
            case DATA_REQUEST_ID.TURN_COORDINATOR:
                turnCoordinator = (gTurnCoordinator)data.dwData[0];
                if (!paused)
                    OnTurnCoordinatorChange(turnCoordinator);
                break;
            case DATA_REQUEST_ID.HEADING_INDICATOR:
                headingIndicator = (gHeadingIndicator)data.dwData[0];
                if (!paused)
                    OnHeadingIndicatorChange(headingIndicator);
                break;
            default:
                OnShowMessage("Other");
                break;
        }
    }
    private void simconnect_OnRecvReservedKey(SimConnect sender, SIMCONNECT_RECV_RESERVED_KEY data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvQuit(SimConnect sender, SIMCONNECT_RECV data)
    {
        OnShowMessage("FSX has exited");
        Dispose();
    }
    private void simconnect_OnRecvOpen(SimConnect sender, SIMCONNECT_RECV_OPEN data)
    {
        OnShowMessage("Connected to FSX");
    }
    private void simconnect_OnRecvNull(SimConnect sender, SIMCONNECT_RECV data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvNdbList(SimConnect sender, SIMCONNECT_RECV_NDB_LIST data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvException(SimConnect sender, SIMCONNECT_RECV_EXCEPTION data)
    {
        global::SimConnect_Exceptions ex = new global::SimConnect_Exceptions();
        OnShowMessage("Exception received: " + data.dwException + ": " + ex.getDescription((SIMCONNECT_EXCEPTION)data.dwException));
    }
    private void simconnect_OnRecvEventWeatherMode(SimConnect sender, SIMCONNECT_RECV_EVENT_WEATHER_MODE data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvEventRaceLap(SimConnect sender, SIMCONNECT_RECV_EVENT_RACE_LAP data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvEventRaceEnd(SimConnect sender, SIMCONNECT_RECV_EVENT_RACE_END data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvEventObjectAddremove(SimConnect sender, SIMCONNECT_RECV_EVENT_OBJECT_ADDREMOVE data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvEventMultiplayerSessionEnded(SimConnect sender, SIMCONNECT_RECV_EVENT_MULTIPLAYER_SESSION_ENDED data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvEventMultiplayerServerStarted(SimConnect sender, SIMCONNECT_RECV_EVENT_MULTIPLAYER_SERVER_STARTED data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvEventMultiplayerClientStarted(SimConnect sender, SIMCONNECT_RECV_EVENT_MULTIPLAYER_CLIENT_STARTED data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvEventFrame(SimConnect sender, SIMCONNECT_RECV_EVENT_FRAME data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvEventFilename(SimConnect sender, SIMCONNECT_RECV_EVENT_FILENAME data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvEvent(SimConnect sender, SIMCONNECT_RECV_EVENT data)
    {
        switch (data.uEventID)
        {
            case (uint)EVENT_ID.EVENT_PAUSE:
                {
                    OnShowMessage("Pause");
                    paused = data.dwData != 0;
                }
                break;
            case (uint)EVENT_ID.EVENT_SIM_START:
                {
                    OnShowMessage("Sim Started");
                }
                break;

            case (uint)EVENT_ID.EVENT_A_Press:
                {
                    OnShowMessage("A");
                    // Increase the throttle
                    //if (struct2.GENERAL_ENG_THROTTLE_LEVER_POSITION_1 <= 95.0f)
                    //{
                    //    struct2.GENERAL_ENG_THROTTLE_LEVER_POSITION_1 += 5.0f;
                    //}
                    //connect.SetDataOnSimObject(DATA_DEFINE_ID.STRUCT_2, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_DATA_SET_FLAG.DEFAULT, struct2);
                }
                break;

            case (uint)EVENT_ID.EVENT_Z_Press:
                {
                    OnShowMessage("Z");
                    // Decrease the throttle
                    //    if (struct2.GENERAL_ENG_MIXTURE_LEVER_POSITION_1 >= 5.0f)
                    //    {
                    //        struct2.GENERAL_ENG_MIXTURE_LEVER_POSITION_1 -= 5.0f;
                    //    }
                    //    //struct2.GENERAL_ENG_MIXTURE_LEVER_POSITION_1 = 50f;
                    //    connect.SetDataOnSimObject(DATA_DEFINE_ID.STRUCT_2, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_DATA_SET_FLAG.DEFAULT, struct2);
                }
                break;

            default:
                break;
        }
    }
    private void simconnect_OnRecvCustomAction(SimConnect sender, SIMCONNECT_RECV_CUSTOM_ACTION data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvClientData(SimConnect sender, SIMCONNECT_RECV_CLIENT_DATA data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvAssignedObjectId(SimConnect sender, SIMCONNECT_RECV_ASSIGNED_OBJECT_ID data)
    {
        throw new NotImplementedException();
    }
    private void simconnect_OnRecvAirportList(SimConnect sender, SIMCONNECT_RECV_AIRPORT_LIST data)
    {
        throw new NotImplementedException();
    }
    #endregion

    #region Here is where you map definitions and subscribe/unsubscribe to events
    void AddToDataDefinitions()
    {
        connect.AddToDataDefinition(DATA_DEFINE_ID.ATTITUDE_INDICATOR, "PLANE PITCH DEGREES", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
        connect.AddToDataDefinition(DATA_DEFINE_ID.ATTITUDE_INDICATOR, "PLANE BANK DEGREES", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
        connect.RegisterDataDefineStruct<gAttitudeIndicator>(DATA_DEFINE_ID.ATTITUDE_INDICATOR);

        connect.AddToDataDefinition(DATA_DEFINE_ID.VERTICAL_SPEED_INDICATOR, "VERTICAL SPEED", "feet/minute", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
        connect.RegisterDataDefineStruct<gVerticalSpeedIndicator>(DATA_DEFINE_ID.VERTICAL_SPEED_INDICATOR);

        connect.AddToDataDefinition(DATA_DEFINE_ID.ALTITUDE_INDICATOR, "PLANE ALTITUDE", "feet", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
        connect.RegisterDataDefineStruct<gAltitudeIndicator>(DATA_DEFINE_ID.ALTITUDE_INDICATOR);

        connect.AddToDataDefinition(DATA_DEFINE_ID.SPEED_INDICATOR, "AIRSPEED TRUE", "knots", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
        connect.RegisterDataDefineStruct<gSpeedIndicator>(DATA_DEFINE_ID.SPEED_INDICATOR);

        //connect.AddToDataDefinition(DATA_DEFINE_ID.TURN_COORDINATOR, "DELTA HEADING RATE", "degrees per minute", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
        connect.AddToDataDefinition(DATA_DEFINE_ID.TURN_COORDINATOR, "TURN INDICATOR RATE", "degrees per second", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
        connect.AddToDataDefinition(DATA_DEFINE_ID.TURN_COORDINATOR, "TURN COORDINATOR BALL", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
        connect.RegisterDataDefineStruct<gTurnCoordinator>(DATA_DEFINE_ID.TURN_COORDINATOR);

        connect.AddToDataDefinition(DATA_DEFINE_ID.HEADING_INDICATOR, "HEADING INDICATOR", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
        connect.AddToDataDefinition(DATA_DEFINE_ID.HEADING_INDICATOR, "AUTOPILOT HEADING LOCK DIR", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0, SimConnect.SIMCONNECT_UNUSED);
        connect.RegisterDataDefineStruct<gHeadingIndicator>(DATA_DEFINE_ID.HEADING_INDICATOR);
    }
    void SubscribeToSystemEvents()
    {
        // Request a simulation started and stop event
        //https://msdn.microsoft.com/en-us/library/cc526983.aspx#SimConnect_SubscribeToSystemEvent
        connect.SubscribeToSystemEvent(EVENT_ID.EVENT_SIM_START, "SimStart");
        connect.SubscribeToSystemEvent(EVENT_ID.EVENT_SIM_STOP, "SimStop");
        connect.SubscribeToSystemEvent(EVENT_ID.EVENT_PAUSE, "Pause");
        //simconnect.SubscribeToSystemEvent(EVENT_ID.EVENT_CustomMissionActionExecuted, "CustomMissionActionExecuted");
    }
    void UnsubscribeFromSystemEvent()
    {
        if (connect != null)
        {
            // Unsubscribe from all the system events
            connect.UnsubscribeFromSystemEvent(EVENT_ID.EVENT_SIM_START);
            connect.UnsubscribeFromSystemEvent(EVENT_ID.EVENT_SIM_STOP);
            connect.UnsubscribeFromSystemEvent(EVENT_ID.EVENT_PAUSE);
        }
    }
    #endregion
}
