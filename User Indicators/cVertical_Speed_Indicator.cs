﻿using SharpDX;
using SharpDX.Direct2D1;

class cVertical_Speed_Indicator
{

    public Bitmap _bmpBack;
    private Bitmap _bmpHand;

    Vector2 center;
    float _angle = 0f;
    float _scale = 1f;
    Vector2 _position;
    public Vector2 position { get { return _position; } set { _position = value; } }
    public float vertical_speed { get { return _angle; } set { _angle = math.InterpolPhyToAngle (value, -2000, 2000, -164, 164); } }
    public float scale { get { return _scale; } set { _scale = value; } }
    public float WidthScaled { get { return _bmpBack.Size.Width*_scale; }}
    public float HeightScaled { get { return _bmpBack.Size.Height * _scale; } }

    cMath math =new cMath();
    public cVertical_Speed_Indicator(RenderTarget device, float Scale)
    {
        _scale = Scale;
        _bmpBack = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.vario_markings);
        _bmpHand = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.vario_hand);
        center = new Vector2(_bmpBack.Size.Width / 2f, _bmpBack.Size.Height / 2f);
    }

    public void Draw(RenderTarget device)
    {
        device.Transform = Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpBack, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Rotation(MathUtil.DegreesToRadians(_angle), center) * Matrix3x2.Translation(_position.X, _position.Y) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpHand, 1, BitmapInterpolationMode.Linear);

    }
}

