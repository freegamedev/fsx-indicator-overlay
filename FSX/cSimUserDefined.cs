﻿using Microsoft.FlightSimulator.SimConnect;
using System;
using System.Runtime.InteropServices;

//public enum GROUP_ID : uint
//{
//    GROUP_KEYS,
//}
//public enum INPUT_ID : uint
//{
//    INPUT_KEYS,
//}

//client defined (can be in any order etc)
public enum EVENT_ID : uint
{
    EVENT_SIM_START,
    EVENT_SIM_STOP,
    EVENT_A_Press,
    EVENT_Z_Press,
    EVENT_A_Release,
    EVENT_Z_Release,
    EVENT_PAUSE
}

//can add any of these https://docs.microsoft.com/en-us/previous-versions/microsoft-esp/cc526981%28v%3dmsdn.10%29
public enum DATA_DEFINE_ID : uint
{
    ATTITUDE_INDICATOR,
    VERTICAL_SPEED_INDICATOR,
    ALTITUDE_INDICATOR,
    SPEED_INDICATOR,
    TURN_COORDINATOR,
    HEADING_INDICATOR,
}

public enum DATA_REQUEST_ID : uint
{
    ATTITUDE_INDICATOR,
    VERTICAL_SPEED_INDICATOR,
    ALTITUDE_INDICATOR,
    SPEED_INDICATOR,
    TURN_COORDINATOR,
    HEADING_INDICATOR,
}

//SimConnect Variables found at this link
//https://docs.microsoft.com/en-us/previous-versions/microsoft-esp/cc526981(v=msdn.10)
[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
public class gAttitudeIndicator
{
    /// <summary>Pitch angle, although the name mentions degrees the units used are radians</summary>
    public double PitchDegrees = 0;
    /// <summary>Bank angle, although the name mentions degrees the units used are radians</summary>
    public double BankDegrees = 0;
}

[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
public class gVerticalSpeedIndicator
{
    public double VerticalSpeed;
}

[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
public class gAltitudeIndicator
{
    public double Altitude;
}

[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
public class gSpeedIndicator
{
    public double Speed;
}

[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
public class gTurnCoordinator
{
    public double Plane;
    public double Ball;
}

[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
public class gHeadingIndicator
{
    public double Heading;
    public double Bug;
}
