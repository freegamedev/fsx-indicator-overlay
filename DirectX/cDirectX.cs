﻿using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DirectWrite;
using System;
using System.Threading;
using Factory = SharpDX.Direct2D1.Factory;
using FontFactory = SharpDX.DirectWrite.Factory;
using Format = SharpDX.DXGI.Format;

class cDirectX
{
    #region declerations
    private WindowRenderTarget device;
    private HwndRenderTargetProperties renderProperties;
    private SolidColorBrush solidColorBrush;
    private Factory factory;

    //text fonts to test DirectX direct draw text
    private TextFormat font;
    private FontFactory fontFactory;
    private const string fontFamily = "Arial";
    private const float fontSize = 18.0f;
    private Bitmap _bitmap;

    private IntPtr handle;
    private Thread threadDX = null;
    #endregion

    public Color transparentColor = Color.Transparent;
    public string DebugString = string.Empty;
    int _width;
    int _height;
    public float scale = 1f;
    public cAttitude_Indicator attitudeIndicator;
    public cVertical_Speed_Indicator verticalSpeedIndicator;
    public cAltitude_Indicator altitudeIndicator;
    public cSpeed_Indicator speedIndicator;
    public cTurn_Coordinator turnCoordinator;
    public cHeadingIndicator headingIndicator;

    /// <summary>Setup DirectX and Starts Loop</summary>
    /// <param name="Handle">handle to form</param>
    /// <param name="width">width of form</param>
    /// <param name="height">height of form</param>
    public cDirectX(IntPtr Handle, int width, int height)
    {
        _width = width;
        _height = height;
        handle = Handle;
        factory = new Factory();
        fontFactory = new FontFactory();
        renderProperties = new HwndRenderTargetProperties()
        {
            Hwnd = Handle,
            PixelSize = new Size2(width, height),
            PresentOptions = PresentOptions.None
        };


        //Init DirectX
        device = new WindowRenderTarget(factory, new RenderTargetProperties(new PixelFormat(Format.B8G8R8A8_UNorm, AlphaMode.Premultiplied)), renderProperties);
        solidColorBrush = new SolidColorBrush(device, Color.Red);
        font = new TextFormat(fontFactory, fontFamily, fontSize);

        threadDX = new Thread(new ParameterizedThreadStart(_loop_DXThread));

        threadDX.Priority = ThreadPriority.Highest;
        threadDX.IsBackground = true;
        threadDX.Start();
    }

    private void _loop_DXThread(object sender)
    {
        device.TextAntialiasMode = SharpDX.Direct2D1.TextAntialiasMode.Default;
        device.AntialiasMode = AntialiasMode.PerPrimitive;
        attitudeIndicator = new cAttitude_Indicator(device,scale);
        float spacingX = attitudeIndicator.WidthScaled;
        float spacingY = attitudeIndicator.HeightScaled;
        attitudeIndicator.position = new Vector2((_width / 2f - attitudeIndicator.WidthScaled / 2f ) / scale, (_height - attitudeIndicator.HeightScaled -spacingY) / scale);
        verticalSpeedIndicator = new cVertical_Speed_Indicator(device,scale);
        verticalSpeedIndicator.position = new Vector2((_width / 2f - verticalSpeedIndicator.WidthScaled / 2f + spacingX) / scale, (_height - verticalSpeedIndicator.HeightScaled) / scale);
        altitudeIndicator = new cAltitude_Indicator(device, scale);
        altitudeIndicator.position = new Vector2((_width / 2f - altitudeIndicator.WidthScaled / 2f + spacingX) / scale, (_height - altitudeIndicator.HeightScaled - spacingY) / scale);
        speedIndicator = new cSpeed_Indicator(device, scale);
        speedIndicator.position = new Vector2((_width / 2f - verticalSpeedIndicator.WidthScaled / 2f - spacingX) / scale, (_height - verticalSpeedIndicator.HeightScaled - spacingY) / scale);
        turnCoordinator = new cTurn_Coordinator(device, scale);
        turnCoordinator.position = new Vector2((_width / 2f - turnCoordinator.WidthScaled / 2f - spacingX) / scale, (_height - turnCoordinator.HeightScaled) / scale);
        headingIndicator = new cHeadingIndicator(device, scale);
        headingIndicator.position = new Vector2((_width / 2f - headingIndicator.WidthScaled / 2f) / scale, (_height - headingIndicator.HeightScaled) / scale);

        while (true)
        {
            device.BeginDraw();
            device.Clear(transparentColor);
            if (DebugString!=string.Empty)
            {
                device.Transform = Matrix3x2.Identity;
                device.DrawText(DebugString, font, new SharpDX.Mathematics.Interop.RawRectangleF(5, 5, _width / 4, _height), solidColorBrush);
            }

            attitudeIndicator.Draw(device);
            verticalSpeedIndicator.Draw(device);
            altitudeIndicator.Draw(device);
            speedIndicator.Draw(device);
            turnCoordinator.Draw(device);
            headingIndicator.Draw(device);
            device.EndDraw();
        }
    }
}