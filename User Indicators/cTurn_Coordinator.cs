﻿using SharpDX;
using SharpDX.Direct2D1;
using System;

class cTurn_Coordinator
{

    public Bitmap _bmpBack;
    private Bitmap _bmpBall;
    private Bitmap _bmpPlane;
    private Bitmap _bmpOverlay;

    Vector2 center;
    float _plane = 0f;
    float _scale = 1f;
    float _ball = 0;
    float _ballY = 0;
    float _ballX = 0;
    Vector2 _position;
    public Vector2 position { get { return _position; } set { _position = value; } }

    public float plane
    {
        get { return _plane; }
        set { _plane = value / 0.16f; } //3 degree per second = 20 degree bank (3/20=0.15) (this indicator is actually 18.4 =20)
    }
    public float ball
    {
        get { return _ball; }
        set
        {
            float radius = _bmpBack.Size.Width / 2f;
            _ball = value;
            _ballX = (float)(Math.Cos(MathUtil.DegreesToRadians(value))) * radius - radius;
            if (_ball < 0f)
            {
                _ballY = _ball * -1;
                _ballX = -_ballX;
            }
            else
                _ballY = _ball;
            _ballY *= -0.05f; //just a hack to make the ball stay in the right area (todo come up with better math)

        }
    }
    public float scale { get { return _scale; } set { _scale = value; } }
    public float WidthScaled { get { return _bmpBack.Size.Width * _scale; } }
    public float HeightScaled { get { return _bmpBack.Size.Height * _scale; } }
    public cTurn_Coordinator(RenderTarget device, float Scale)
    {
        _scale = Scale;
        _bmpBack = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.turn_markings_1);
        _bmpBall = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.turn_ball);
        _bmpPlane = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.turn_airplane);
        _bmpOverlay = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.turn_markings_2);
        center = new Vector2(_bmpBack.Size.Width / 2, _bmpBack.Size.Height / 2);
    }

    public void Draw(RenderTarget device)
    {
        device.Transform = Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpBack, 1, BitmapInterpolationMode.Linear);
        //device.Transform = Matrix3x2.Rotation(MathUtil.DegreesToRadians(_ball), center) * Matrix3x2.Translation(new Vector2(_position.X,_position.Y+ _ballY)) * Matrix3x2.Scaling(_scale);
        device.Transform = Matrix3x2.Translation(new Vector2(_position.X + _ballX, _position.Y +_ballY)) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpBall, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Rotation(MathUtil.DegreesToRadians(_plane), center) * Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpPlane, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpOverlay, 1, BitmapInterpolationMode.Linear);
    }
}