﻿using SharpDX;
using SharpDX.Direct2D1;

class cHeadingIndicator
{

    public Bitmap _bmpBack;
    private Bitmap _bmpHand;
    private Bitmap _bmpBug;

    Vector2 center;
    float _angle = 0f;
    float _scale = 1f;
    float _headingBug = 0f;
    Vector2 _position;
    public Vector2 position { get { return _position; } set { _position = value; } }
    public float Heading{ get { return _angle; }set {_angle = value;}}
    public float HeadingBug { get { return _headingBug; } set { _headingBug = value; } }
    public float scale { get { return _scale; } set { _scale = value; } }
    public float WidthScaled { get { return _bmpBack.Size.Width * _scale; } }
    public float HeightScaled { get { return _bmpBack.Size.Height * _scale; } }

    cMath math = new cMath();
    public cHeadingIndicator(RenderTarget device, float Scale)
    {
        _scale = Scale;
        _bmpBack = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.heading_yaw );
        _bmpHand = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.heading_markings);
        _bmpBug = cLoadBitmap.FromBitmap(device, FSX_Indicators.Properties.Resources.heading_beacon_2);
        center = new Vector2(_bmpBack.Size.Width / 2f, _bmpBack.Size.Height / 2f);
    }

    public void Draw(RenderTarget device)
    {
        device.Transform = Matrix3x2.Rotation(MathUtil.DegreesToRadians(_angle), center) * Matrix3x2.Translation(_position.X, _position.Y) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpBack, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Translation(_position) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpHand, 1, BitmapInterpolationMode.Linear);
        device.Transform = Matrix3x2.Rotation(MathUtil.DegreesToRadians(_angle + _headingBug), center) * Matrix3x2.Translation(_position.X, _position.Y) * Matrix3x2.Scaling(_scale);
        device.DrawBitmap(_bmpBug, 1, BitmapInterpolationMode.Linear);

    }
}