# FSX Indicator Overlay
  
*Important: To close program double click any indicator..    
    
This project connects to Microsoft Flight Simulator's simConnect and creates a transparent window overlay with 6 of the most common aviation indicators.    
Can be used with another rendering engine such as google earth with [HttpX.dll and EarthProxy.exe](https://www.fsdeveloper.com/forum/threads/ge-view-64-bit-httpx-v2-2-and-earthproxy-v1-3-fsx-thru-p3d-v4.442397/) tool.
    
    
Special Thanks to the following projects for the indicator images.    
[jQuery-Flight-Indicators](https://github.com/sebmatton/jQuery-Flight-Indicators)    
[Skyhawk-Flight-Instruments](https://github.com/CDOT-EDX/Skyhawk-Flight-Instruments)    
![picture](_ScreenShot/FSXIndicatorOverlay.jpg) 